package pl.umk.mat.trapp.training;

import pl.umk.mat.trapp.exercise.Exercise;
import pl.umk.mat.trapp.exercise.ExerciseDto;
import pl.umk.mat.trapp.user.UserDto;

import java.util.List;
import java.util.stream.Collectors;

public class TrainingDto {

    private final Long id;
    private final String name;
    private final UserDto userDto;
    private final List<ExerciseDto> exercises;

    public TrainingDto(Training training) {
        this.id = training.getId();
        this.name = training.getName();
        this.userDto = new UserDto(training.getUser());
        this.exercises = training.getExercises().stream().map(ExerciseDto::new).collect(Collectors.toList());
    }

    public List<ExerciseDto> getExercises() {
        return exercises;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public UserDto getUserDto() {
        return userDto;
    }

}
