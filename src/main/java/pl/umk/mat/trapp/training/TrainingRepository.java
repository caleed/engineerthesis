package pl.umk.mat.trapp.training;

import pl.umk.mat.trapp.user.User;
import pl.umk.mat.trapp.utils.BaseRepository;

import java.util.List;

public interface TrainingRepository extends BaseRepository<Training, Long> {

    List<Training> getAllByUserId(Long id);

    Training findTrainingByUserAndId(User user, Long id);

    void deleteByUserAndId(User user, Long id);

}
