package pl.umk.mat.trapp.training;


import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin()
@RestController
@RequestMapping("/trainings")
public class TrainingController {


    private final TrainingService trainingService;
    public TrainingController(TrainingService trainingService) {
        this.trainingService = trainingService;
    }

    @GetMapping
    @ApiOperation(
            value = "Return All Trainings",
            authorizations = @Authorization(
                    "Authorization"
            )
    )
    public List<TrainingDto> getAllTrainings() {
        return trainingService.getAllTrainings();
    }


    @GetMapping("/all/")
    @ApiOperation(
            value = "Get all training from user",
            authorizations = @Authorization(
                    "Authorization"
            )
    )
    public List<TrainingDto> getAllTrainingsByUserId() {
        return trainingService.getAllTrainingsByUserId();
    }


    @PostMapping
    @ApiOperation(
            value = "Add new training from User",
            authorizations = @Authorization(
                    "Authorization"
            )
    )
    public TrainingDto AddTraining(@RequestBody NewTraining newTraining) {
        return trainingService.addTraining(newTraining);
    }

    @GetMapping("/{id}")
    @ApiOperation(
            value = "Get Training from User with Training id",
            authorizations = @Authorization(
                    "Authorization"
            )
    )
    public TrainingDto getTrainingById(@PathVariable Long id) {
        return trainingService.findTrainingByUserAndId(id);
    }

    @Transactional
    @DeleteMapping("/delete/{id}")
    @ApiOperation(
            value = "Delete User Traning",
            authorizations = @Authorization(
                    "Authorization"
            )
    )
    public void deleteTraining(@PathVariable Long id){
        trainingService.delete(id);
    }


    @PutMapping("/{id}")
    @ApiOperation(
            value = "Edit User Training",
            authorizations = @Authorization(
                    "Authorization"
            )
    )
    public void editTraining(@PathVariable Long id, @RequestBody NewTraining newTraining){
        trainingService.editTraining(id,newTraining);
    }



}
