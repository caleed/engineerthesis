package pl.umk.mat.trapp.training;

import pl.umk.mat.trapp.exercise.ExerciseDto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

public class NewTraining {
    @NotBlank
    @Size(max = 200)
    private String name;
    private List<ExerciseDto> exercises;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ExerciseDto> getExercises() {
        return exercises;
    }

    public void setExercises(List<ExerciseDto> exercises) {
        this.exercises = exercises;
    }

}
