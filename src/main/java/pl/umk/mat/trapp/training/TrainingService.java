package pl.umk.mat.trapp.training;


import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.umk.mat.trapp.auth.LoggedUserHolder;
import pl.umk.mat.trapp.exercise.Exercise;
import pl.umk.mat.trapp.sets.Sets;

import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Service
public class TrainingService {

    private final TrainingRepository trainingRepository;
    private final LoggedUserHolder loggedUserHolder;

    public TrainingService(TrainingRepository trainingRepository, LoggedUserHolder loggedUserHolder) {
        this.trainingRepository = trainingRepository;
        this.loggedUserHolder = loggedUserHolder;
    }

    public List<TrainingDto> getAllTrainings() {
        return trainingRepository.findAllList().stream().map(TrainingDto::new).collect(Collectors.toList());
    }

    public List<TrainingDto> getAllTrainingsByUserId() {
        var loggedUser = loggedUserHolder.getLoggedUser();
        return trainingRepository.getAllByUserId(loggedUser.getUserId()).stream().map(TrainingDto::new).collect(Collectors.toList());
    }

    @Transactional
    public TrainingDto addTraining(NewTraining newTraining) {
        var loggedUser = loggedUserHolder.getLoggedUser().getUser();
        Training training = new Training(
                newTraining.getName(),
                loggedUser,
                newTraining.getExercises().stream().map(it -> new Exercise(
                        it.getName(),
                        it.getSets().stream().map(it2 -> new Sets(it2.getRepActual(),it2.getWeight()
                                , it2.getSortOrder())).collect(Collectors.toList())
                )).collect(Collectors.toList())
        );

        return new TrainingDto(trainingRepository.save(training));
    }

    public TrainingDto findTrainingByUserAndId(Long id) {
        var loggedUser = loggedUserHolder.getLoggedUser().getUser();
        return new TrainingDto(trainingRepository.findTrainingByUserAndId(loggedUser, id));
    }

    public void delete(Long id){
        var loggedUser = loggedUserHolder.getLoggedUser().getUser();
        trainingRepository.deleteByUserAndId(loggedUser,id);
    }


    public void editTraining(Long  id, NewTraining newTraining){
        var loggedUser = loggedUserHolder.getLoggedUser().getUser();
        Training training = trainingRepository.findTrainingByUserAndId(loggedUser,id);
        training.setName(newTraining.getName());
        training.setExercises(newTraining.getExercises().stream().map(it -> new Exercise(
                it.getName(),
                it.getSets().stream().map(it2 -> new Sets(it2.getRepActual(),it2.getWeight(),
                        it2.getSortOrder())).collect(Collectors.toList())
        )).collect(Collectors.toList()));
        trainingRepository.save(training);
    }



}
