package pl.umk.mat.trapp.training;

import org.hibernate.annotations.*;
import pl.umk.mat.trapp.auth.UserPrincipal;
import pl.umk.mat.trapp.exercise.Exercise;
import pl.umk.mat.trapp.exercise.ExerciseDto;
import pl.umk.mat.trapp.user.User;
import pl.umk.mat.trapp.user.UserDto;
import pl.umk.mat.trapp.utils.BaseEntity;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;

@Entity
@SQLDelete(sql="UPDATE training SET deleted = 1 WHERE id = ?  and version=?")
@Where(clause = "deleted = false")
public class Training extends BaseEntity {

    private String name;

    @ManyToOne
    private User user;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "training_id")
    private List<Exercise> exercises = new ArrayList<>();

    private boolean deleted;

    public Training(String name, User user, List<Exercise> exercises) {
        this.name = name;
        this.user = user;
        this.exercises = exercises;
    }

    public Training(String name, User user) {
        this.name = name;
        this.user = user;
    }

    public Training() {
        //JPA
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Exercise> getExercises() {
        return exercises;
    }

    public void setExercises(List<Exercise> exercises) {
        this.exercises = exercises;
    }
}
