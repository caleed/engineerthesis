package pl.umk.mat.trapp.exercise;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.umk.mat.trapp.config.AppConfig;
import pl.umk.mat.trapp.sets.SetsDto;
import pl.umk.mat.trapp.tags.TagDto;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/exercise")
public class ExerciseController {

    private final ExerciseRepository exerciseRepository;
    private final ExerciseService exerciseService;
    private AppConfig config;

    public ExerciseController(ExerciseRepository exerciseRepository, ExerciseService exerciseService, AppConfig config) {
        this.exerciseRepository = exerciseRepository;
        this.exerciseService = exerciseService;
        this.config = config;
    }

    @GetMapping
    @ApiOperation(
            value = "Returns all Excersise",
            authorizations = @Authorization(
                    "Authorization"
            )
    )
    public List<ExerciseDto> getAllExercises(){
        return exerciseService.getAllExercises();
    }

    @PostMapping
    @ApiOperation(
            value = "Add Exercise to Training",
            authorizations = @Authorization(
                    "Authorization"
            )
    )
    public ExerciseDto addExercise(@RequestBody NewExercise newExercise) {
        return  exerciseService.addExercise(newExercise);
    }

    @GetMapping("/suggestions")
    @ApiOperation(
            value = "Returns all Excersise",
            authorizations = @Authorization(
                    "Authorization"
            )
    )
    public List<ExerciseDto> suggest(@RequestParam List<Long> tagIds){
         return exerciseService.test(tagIds);
    }

    @ApiOperation(value = "")
    @GetMapping("/{imageName}")
    @ResponseBody
    public HttpEntity<byte[]> getPhoto(
            @PathVariable String imageName
    ) throws IOException {
        byte[] image = Files.readAllBytes(Paths.get(config.getImageDir() + imageName));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_PNG);
        headers.setContentLength(image.length);
        return new HttpEntity<byte[]>(image, headers);
    }

    @GetMapping("/suggestExercise")
    @ApiOperation(
            value = "Returns all Excersise Name",
            authorizations = @Authorization(
                    "Authorization"
            )
    )

    public List<ExerciseService.Wrapper> suggestExercise(){
        return exerciseService.suggestExercise();
    }


    @GetMapping("/userExercises")
    @ApiOperation(
            value = "Returns user Exercises",
            authorizations = @Authorization(
                    "Authorization"
            )
    )
    public List<ExerciseDto> getUserExercises(){
        return exerciseService.getAllExerciseByUser();
    }


    @GetMapping("/userExercisesTags")
    @ApiOperation(
            value = "Returns user Tags Name and Count of Exercise",
            authorizations = @Authorization(
                    "Authorization"
            )
    )
    public List<ExerciseService.GraphData> getTagsFromUserExercises(){
        return exerciseService.getTagsFromUserExercises();
    }


}
