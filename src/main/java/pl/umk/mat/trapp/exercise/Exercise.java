package pl.umk.mat.trapp.exercise;

import org.hibernate.annotations.Formula;
import pl.umk.mat.trapp.sets.Sets;
import pl.umk.mat.trapp.tags.Tag;
import pl.umk.mat.trapp.training.Training;
import pl.umk.mat.trapp.user.User;
import pl.umk.mat.trapp.utils.BaseEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Exercise extends BaseEntity {
    private String name;

    @ManyToOne
    private Training training;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "exercise_id")
    private List<Sets> sets = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(
            name = "exercise_tags",
            joinColumns = @JoinColumn(name = "exercise_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private List<Tag> tags = new ArrayList<>();

    private Double recordMax;

    @ManyToOne
    private User user;

    public Exercise(String name) {
        this.name = name;
    }

    public Exercise(String name, Training training, List<Sets> sets, User user) {
        this.name = name;
        this.training = training;
        this.sets = sets;
        this.user = user;
        this.recordMax  = 0.0;
    }

    public Exercise(String name, List<Sets> sets) {
        this.name = name;
        this.sets = sets;
    }


    public Exercise() {
        //JPA
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Training getTraining() {
        return training;
    }

    public List<Sets> getSets() {
        return sets;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public Double getRecordMax() {
        return recordMax;
    }

    public void setRecordMax(Double recordMax) {
        this.recordMax = recordMax;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
