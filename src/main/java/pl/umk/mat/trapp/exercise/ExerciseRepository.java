package pl.umk.mat.trapp.exercise;

import pl.umk.mat.trapp.training.Training;
import pl.umk.mat.trapp.utils.BaseRepository;

import java.util.List;

public interface ExerciseRepository extends BaseRepository<Exercise, Long> {

    List<Exercise> getAllByUserId(Long id);
}
