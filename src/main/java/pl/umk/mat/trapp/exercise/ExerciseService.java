package pl.umk.mat.trapp.exercise;

import org.springframework.stereotype.Service;
import pl.umk.mat.trapp.auth.LoggedUserHolder;
import pl.umk.mat.trapp.sets.SetsService;
import pl.umk.mat.trapp.tags.Tag;
import pl.umk.mat.trapp.tags.TagDto;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Tuple;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ExerciseService {

    private final ExerciseRepository exerciseRepository;
    private final LoggedUserHolder loggedUserHolder;
    private final SetsService setsService;

    @PersistenceContext
    private EntityManager entityManager;

    public ExerciseService(ExerciseRepository exerciseRepository, LoggedUserHolder loggedUserHolder, SetsService setsService) {
        this.exerciseRepository = exerciseRepository;
        this.loggedUserHolder = loggedUserHolder;
        this.setsService = setsService;
    }

    public List<ExerciseDto> getAllExercises() {
        return exerciseRepository.findAllList().stream().map(ExerciseDto::new).collect(Collectors.toList());
    }

    public List<ExerciseDto> getAllExerciseByUser() {
        var loggedUser = loggedUserHolder.getLoggedUser();
        return exerciseRepository.getAllByUserId(loggedUser.getUserId()).stream().map(ExerciseDto::new).collect(Collectors.toList());
    }

    public ExerciseDto addExercise(NewExercise newExercise) {
        var loggedUser = loggedUserHolder.getLoggedUser().getUser();
        Exercise exercise = new Exercise(
                newExercise.getName(),
                newExercise.getTraining(),
                newExercise.getSets(),
                loggedUser
        );
        exerciseRepository.save(exercise);
        return new ExerciseDto(exercise);
    }


    public List<ExerciseDto> test(List<Long> tagIds) {
        var tags = new ArrayList<Tag>();
        var query = "SELECT e as tmp FROM Exercise e LEFT JOIN e.tags t GROUP BY e ORDER BY SUM((CASE WHEN t.id IN :ids THEN 1 ELSE 0 END)) DESC";

        return entityManager.createQuery(query, Exercise.class)
                .setParameter("ids", tagIds).setMaxResults(2)
                .getResultList().stream().map(ExerciseDto::new).collect(Collectors.toList());
    }

    public List<Wrapper> suggestExercise() {
        var loggedUser = loggedUserHolder.getLoggedUser();
        var ex = exerciseRepository.findAllList().stream().collect(Collectors.groupingBy(Exercise::getName));
        var out = new ArrayList<Wrapper>();
        for (var exerciseName : ex.keySet()) {
            out.add(new Wrapper(exerciseName, ex.get(exerciseName).stream().map(Exercise::getTags).flatMap(Collection::stream).distinct().map(TagDto::new).collect(Collectors.toList())));
        }
        return out;
    }

    static public class Wrapper {
        private String name;
        private List<TagDto> tags;

        public Wrapper(String name, List<TagDto> tags) {
            this.name = name;
            this.tags = tags;
        }

        public String getName() {
            return name;
        }

        public List<TagDto> getTags() {
            return tags;
        }
    }

    public List<GraphData> getTagsFromUserExercises() {
        var loggedUser = loggedUserHolder.getLoggedUser().getUserId();

        var query = "SELECT t.name as tagName,(SELECT COUNT(*) FROM exercise e JOIN exercise_tags et ON e.id = et.exercise_id WHERE et.tag_id =t.id and e.user_id= :ids ) as count2 From tag t where id IN (Select tag_id FROM exercise_tags Where exercise_id IN (Select id FROM exercise where user_id = :ids))";

        var x =  (List<GraphData>)entityManager.createNativeQuery(query, Tuple.class)
                .setParameter("ids", loggedUser).getResultList().stream().map(it -> new GraphData(
                        ((Tuple)it).get("tagName", String.class), ((Tuple)it).get("count2", BigInteger.class).intValue())
                ).collect(Collectors.toList());
        return x;
    }


    static public class GraphData{
        private String tagName;
        private int count2;

        public GraphData(String tagName, int count2) {
            this.tagName = tagName;
            this.count2 = count2;
        }
        public GraphData() {

        }

        public String getTagName() {
            return tagName;
        }

        public int getCount2() {
            return count2;
        }
    }

}

    //SELECT t.name ,(SELECT COUNT(*) FROM exercise e JOIN exercise_tags et ON e.id = et.exercise_id WHERE et.tag_id =t.id and e.user_id=1 ) as count2 From tag t where id IN (Select tag_id FROM exercise_tags Where exercise_id IN (Select id FROM exercise where user_id = 1))
