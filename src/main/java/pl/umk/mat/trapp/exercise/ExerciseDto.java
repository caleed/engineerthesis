package pl.umk.mat.trapp.exercise;

import pl.umk.mat.trapp.sets.Sets;
import pl.umk.mat.trapp.sets.SetsDto;
import pl.umk.mat.trapp.tags.Tag;
import pl.umk.mat.trapp.tags.TagDto;
import pl.umk.mat.trapp.training.Training;

import java.util.List;
import java.util.stream.Collectors;

public class ExerciseDto {
    private Long id;
    private String name;
    private List<TagDto> tagDto;
    private List<SetsDto> sets;
    private Double recordMax;

    public ExerciseDto(Exercise e){
        this.id = e.getId();
        this.name = e.getName();
        this.tagDto = e.getTags().stream().map(TagDto::new).collect(Collectors.toList());
        this.sets = e.getSets().stream().map(SetsDto::new).collect(Collectors.toList());
        this.recordMax = e.getRecordMax();
    }

    public ExerciseDto(){
    }

    public ExerciseDto(String name, List<SetsDto> sets) {
        this.name = name;
        this.sets = sets;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TagDto> getTagDto() {
        return tagDto;
    }

    public List<SetsDto> getSets() {
        return sets;
    }
}
