package pl.umk.mat.trapp.exercise;

import pl.umk.mat.trapp.sets.Sets;
import pl.umk.mat.trapp.training.Training;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

public class NewExercise {

    @NotBlank
    @Size(max = 30)
    private String name;

    @NotBlank
    private Training training;

    @NotBlank
    private List<Sets> sets;

    public String getName() {
        return name;
    }

    public Training getTraining() {
        return training;
    }

    public List<Sets> getSets() {
        return sets;
    }
}
