package pl.umk.mat.trapp.user;

import pl.umk.mat.trapp.training.Training;
import pl.umk.mat.trapp.utils.BaseRepository;


public interface UserRepository extends BaseRepository<User, Long> {
    User findUserByEmail(String email);

    Boolean existsByEmail(String email);

    User findUserById(Long id);

    void deleteById(Long id);

}
