package pl.umk.mat.trapp.user;

import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.umk.mat.trapp.auth.LoggedUserHolder;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final LoggedUserHolder loggedUserHolder;
    @Lazy
    private final PasswordEncoder passwordEncoder;

    public UserService(UserRepository userRepository, LoggedUserHolder loggedUserHolder, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.loggedUserHolder = loggedUserHolder;
        this.passwordEncoder = passwordEncoder;
    }

    public User findUserByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }

    @Transactional
    public void delete(long id) {
        userRepository.deleteById(id);
    }

    public void editUser(UserDto userDto){
        var loggedUser = loggedUserHolder.getLoggedUser().getUser().getId();
        User user  = userRepository.findUserById(loggedUser);
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        user.setGender(userDto.getGender());
        userRepository.save(user);
    }

    public void editPassword(String password){
        var loggedUser = loggedUserHolder.getLoggedUser().getUser().getId();
        User user  = userRepository.findUserById(loggedUser);
        user.setPassword(passwordEncoder.encode(password));
        userRepository.save(user);
    }



}
