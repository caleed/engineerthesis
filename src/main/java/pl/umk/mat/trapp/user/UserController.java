package pl.umk.mat.trapp.user;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@CrossOrigin()
@RestController
@RequestMapping("/users")
public class UserController {

    public UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{email}")
    public UserDto getUserByEmail(@PathVariable String email) {
        return new UserDto(userService.findUserByEmail(email));
    }


    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(
            @PathVariable long id
    ) {
        userService.delete(id);
    }


    @PutMapping("/edit")
    @ApiOperation(
            value = "Edit User Data",
            authorizations = @Authorization(
                    "Authorization"
            )
    )
    public void editUser(@RequestBody UserDto userDto){
        userService.editUser(userDto);
    }

    @PutMapping("/password")
    @ApiOperation(
            value = "Edit User Data",
            authorizations = @Authorization(
                    "Authorization"
            )
    )
    public void editPassword(@RequestBody String password){
        userService.editPassword(password);
    }



}
