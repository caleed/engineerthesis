package pl.umk.mat.trapp.user;

import pl.umk.mat.trapp.utils.BaseRepository;

import java.util.Optional;

public interface LoginEntryRepository extends BaseRepository<LoginEntry, Long> {
    Optional<LoginEntry> findByToken(String token);
}
