package pl.umk.mat.trapp.user;

import pl.umk.mat.trapp.enums.Gender;
import pl.umk.mat.trapp.enums.Role;
import pl.umk.mat.trapp.sizes.Sizes;
import pl.umk.mat.trapp.training.Training;
import pl.umk.mat.trapp.utils.BaseEntity;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.List;

@Entity
public class User extends BaseEntity {

    private String firstName;

    private String lastName;

    @Email
    private String email;

    private String password;

    private double weight;

    @Enumerated(EnumType.STRING)
    private Role role;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    private boolean isBaned;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private List<Training> trainings;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private List<Sizes> sizes;

    
    public User(String firstName, String lastName, String email, String password, Gender gender, double weight) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.gender = gender;
        this.role = Role.USER;
        this.isBaned = false;
        this.weight = weight;
    }

    protected User() {
        //JPA
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public boolean isBaned() {
        return isBaned;
    }

    public void setBaned(boolean baned) {
        isBaned = baned;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
