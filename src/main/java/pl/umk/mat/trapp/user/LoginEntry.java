package pl.umk.mat.trapp.user;

import pl.umk.mat.trapp.utils.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.time.Instant;

@Entity
public class LoginEntry extends BaseEntity {

    private String token;

    private Instant expireAt;

    @ManyToOne
    private User user;

    protected LoginEntry() {
        // JPA
    }

    public LoginEntry(String token, Instant expireAt, User user) {
        this.token = token;
        this.expireAt = expireAt;
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public String getToken() {
        return token;
    }

    public Instant getExpireAt() {
        return expireAt;
    }

    public void setExpireAt(Instant expireAt) {
        this.expireAt = expireAt;
    }

    public boolean isExpired() {
        return Instant.now().isAfter(expireAt);
    }
}
