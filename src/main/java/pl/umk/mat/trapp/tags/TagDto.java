package pl.umk.mat.trapp.tags;

public class TagDto {
    private Long id;
    private String name;
    private double multiplier;

    public TagDto(Long id,String name, double multiplier) {
        this.id = id;
        this.name = name;
        this.multiplier = multiplier;
    }

    public TagDto(Tag tag)
    {
        this.id = tag.getId();
        this.name=tag.getName();
        this.multiplier=tag.getMultiplier();
    }

    public double getMultiplier() {
        return multiplier;
    }

    public String getName() {
        return name;
    }

    public Long getId() {
        return id;
    }
}
