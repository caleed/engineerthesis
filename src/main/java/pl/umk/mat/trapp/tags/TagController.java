package pl.umk.mat.trapp.tags;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.umk.mat.trapp.training.TrainingDto;
import pl.umk.mat.trapp.training.TrainingService;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/tags")
public class TagController {

    private final TagService tagService;
    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    @GetMapping
    @ApiOperation(
            value = "Return All tags",
            authorizations = @Authorization(
                    "Authorization"
            )
    )
    public List<TagDto> getAll() {
        return tagService.getAllTags();
    }

}
