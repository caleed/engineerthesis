package pl.umk.mat.trapp.tags;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import pl.umk.mat.trapp.defaultExercises.DefaultExercises;
import pl.umk.mat.trapp.exercise.Exercise;
import pl.umk.mat.trapp.utils.BaseEntity;

import javax.persistence.*;
import java.util.List;

@Entity
public class Tag extends BaseEntity {

    private String name;
    private double multiplier;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "exercise_tags",
            joinColumns = @JoinColumn(name = "tag_id"),
            inverseJoinColumns = @JoinColumn(name = "exercise_id")
    )
    private List<Exercise> exercise;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "default_exercise_tags",
            joinColumns = @JoinColumn(name = "tag_id"),
            inverseJoinColumns = @JoinColumn(name = "default_exercise_id")
    )
    private List<DefaultExercises> defaultExercises;



    public Tag(String name, double multiplier) {
        this.name = name;
        this.multiplier = multiplier;
    }

    public Tag() {
        //JPA
    }

    public String getName() {
        return name;
    }

    public double getMultiplier() {
        return multiplier;
    }



}

