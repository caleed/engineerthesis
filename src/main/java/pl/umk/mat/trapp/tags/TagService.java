package pl.umk.mat.trapp.tags;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TagService {

    private TagRepository tagRepository;

    public TagService(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    public List<TagDto> getAllTags(){
       return tagRepository.findAllList().stream().map(TagDto::new).collect(Collectors.toList());
    }
}
