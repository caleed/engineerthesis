package pl.umk.mat.trapp.tags;

import pl.umk.mat.trapp.utils.BaseRepository;

import java.util.List;

public interface TagRepository extends BaseRepository<Tag,Long> {

}
