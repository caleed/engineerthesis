package pl.umk.mat.trapp.sets;

import pl.umk.mat.trapp.defaultExercises.DefaultExercises;
import pl.umk.mat.trapp.exercise.Exercise;
import pl.umk.mat.trapp.utils.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;


@Entity
public class Sets extends BaseEntity {

    private double weight;
    private int repActual;
    private int sortOrder;

    @ManyToOne
    private Exercise exercise;

    @ManyToOne
    private DefaultExercises defaultExercises;

    public Sets(){
        //JPA
    }

    public Sets( int repActual, double weight, int order) {
        this.weight = weight;
        this.repActual = repActual;
        this.sortOrder = order;
    }


    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getRepActual() {
        return repActual;
    }

    public void setRepActual(int repActual) { this.repActual = repActual; }

    public int getSortOrder() {
        return sortOrder;
    }

    public Exercise getExercise() {
        return exercise;
    }

    public DefaultExercises getDefaultExercises() {
        return defaultExercises;
    }
}
