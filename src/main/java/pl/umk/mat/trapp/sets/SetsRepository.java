package pl.umk.mat.trapp.sets;

import pl.umk.mat.trapp.training.Training;
import pl.umk.mat.trapp.utils.BaseRepository;

import java.util.List;

public interface SetsRepository extends BaseRepository<Sets, Long> {

    List<Sets> getAllByExerciseId(Long id);
}
