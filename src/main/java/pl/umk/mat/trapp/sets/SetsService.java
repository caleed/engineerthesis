package pl.umk.mat.trapp.sets;

import org.springframework.stereotype.Service;
import pl.umk.mat.trapp.auth.LoggedUserHolder;
import pl.umk.mat.trapp.exercise.Exercise;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SetsService {

    private final SetsRepository setsRepository;
    private final LoggedUserHolder loggedUserHolder;
    @PersistenceContext
    private EntityManager entityManager;



    public SetsService(SetsRepository setsRepository,LoggedUserHolder loggedUserHolder) {
        this.setsRepository = setsRepository;
        this.loggedUserHolder = loggedUserHolder;
    }

    public List<SetsDto> getAllSets(Long id){
       return setsRepository.getAllByExerciseId(id).stream().map(SetsDto::new).collect(Collectors.toList());
    }

    public List<SetsDto> exerciseSeries(String named){
        var loggedUser = loggedUserHolder.getLoggedUser();
        var query = "Select s From Sets s WHERE (s.exercise.id IN (SELECT e.id FROM Exercise e WHERE e.name = :name AND e.user.id = :ids))";
        return entityManager.createQuery(query, Sets.class).setParameter("ids", loggedUser.getUserId()).setParameter("name", named)
                .getResultList().stream().map(SetsDto::new).collect(Collectors.toList());
    }

    public Double findMax(String name){
       var maximum = exerciseSeries(name).stream().max((o1, o2) -> (int) (o1.getWeight() - o2.getWeight()));
        if (maximum.isEmpty()) {
            return 0.0;
        }
        return maximum.get().getWeight() / (1.0278 - 0.0278 * maximum.get().getRepActual());
    }

}
