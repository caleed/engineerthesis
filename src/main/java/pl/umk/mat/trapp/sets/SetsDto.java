package pl.umk.mat.trapp.sets;

import pl.umk.mat.trapp.exercise.Exercise;

import java.util.Set;

public class SetsDto {
    private Long id;
    private double weight;
    private int repActual;
    private int sortOrder;

    public SetsDto(Long id, double weight, int repActual, int sortOrder) {
        this.id = id;
        this.weight = weight;
        this.repActual = repActual;
        this.sortOrder = sortOrder;
    }

    public SetsDto(Sets s)
    {
        this.id= s.getId();
        this.weight = s.getWeight();
        this.repActual = s.getRepActual();
        this.sortOrder = s.getSortOrder();
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getRepActual() {
        return repActual;
    }

    public void setRepActual(int repActual) {
        this.repActual = repActual;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int order) {
        this.sortOrder = order;
    }
}
