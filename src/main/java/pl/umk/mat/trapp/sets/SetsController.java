package pl.umk.mat.trapp.sets;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.web.bind.annotation.*;
import pl.umk.mat.trapp.training.TrainingDto;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/sets")
public class SetsController {

    private final SetsService setsService;

    public SetsController(SetsService setsService) {
        this.setsService = setsService;
    }

    @GetMapping("/{id}")
    @ApiOperation(
            value ="Get sets from exercise",
            authorizations = @Authorization(
                    "Authorization"
            )
    )
    public List<SetsDto> getAllSetsFromExercise(@PathVariable Long id){
        return setsService.getAllSets(id);
    }


    @GetMapping("/ExerciseSeries/{named}")
    @ApiOperation(value = "Returns sets for All exercise with name",
            authorizations = @Authorization(
                    "Authorization"
            )
    )
    public List<SetsDto> getSetsForName(@PathVariable String named){
        return setsService.exerciseSeries(named);
    }

    @GetMapping("/1RM/{named}")
    @ApiOperation(value = "Returns sets for All exercise with name",
            authorizations = @Authorization(
                    "Authorization"
            )
    )
    public double getOneMax(@PathVariable String named){
        return setsService.findMax(named);
    }

}
