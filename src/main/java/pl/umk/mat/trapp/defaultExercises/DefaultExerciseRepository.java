package pl.umk.mat.trapp.defaultExercises;

import pl.umk.mat.trapp.utils.BaseRepository;

import java.util.List;

public interface DefaultExerciseRepository extends BaseRepository<DefaultExercises, Long> {

}
