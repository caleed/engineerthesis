package pl.umk.mat.trapp.defaultExercises;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.web.bind.annotation.*;
import pl.umk.mat.trapp.tags.TagDto;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/defaultExercises")
public class DefaultExercisesController {

    private final DefaultExerciseRepository defaultExerciseRepository;
    private final DefaultExerciseService defaultExerciseService;

    public DefaultExercisesController(DefaultExerciseRepository defaultExerciseRepository, DefaultExerciseService defaultExerciseService) {
        this.defaultExerciseRepository = defaultExerciseRepository;
        this.defaultExerciseService = defaultExerciseService;
    }

    @GetMapping
    @ApiOperation(
            value = "Returns all Excersise",
            authorizations = @Authorization(
                    "Authorization"
            )
    )
    public List<DefaultExercisesDto> getAllExercises(){
        return defaultExerciseService.getAllExercises();
    }

    @GetMapping("/generate")
    @ApiOperation(
            value = "Returns Exercises with chosen tags",
            authorizations = @Authorization(
                    "Authorization"
            )
    )
    public List<DefaultExercisesDto> generateExercise(@RequestParam List<Long> tags){
        return defaultExerciseService.getExercisesWithTags(tags);
    }

}
