package pl.umk.mat.trapp.defaultExercises;

import org.springframework.stereotype.Service;
import pl.umk.mat.trapp.auth.LoggedUserHolder;
import pl.umk.mat.trapp.sets.SetsService;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DefaultExerciseService {

    private final DefaultExerciseRepository defaultExerciseRepository;
    private final LoggedUserHolder loggedUserHolder;
    private final SetsService setsService;
    private final EntityManager entityManager;

    public DefaultExerciseService(DefaultExerciseRepository defaultExerciseRepository, LoggedUserHolder loggedUserHolder, SetsService setsService, EntityManager entityManager) {
        this.defaultExerciseRepository = defaultExerciseRepository;
        this.loggedUserHolder = loggedUserHolder;
        this.setsService = setsService;
        this.entityManager = entityManager;
    }

    public List<DefaultExercisesDto> getAllExercises() {
        return defaultExerciseRepository.findAllList().stream().map(DefaultExercisesDto::new).collect(Collectors.toList());
    }


    public List<DefaultExercisesDto> getExercisesWithTags(List<Long> tagIds) {

        var query = "SELECT e as tmp FROM DefaultExercises e LEFT JOIN e.tags t GROUP BY e ORDER BY SUM((CASE WHEN t.id IN :ids THEN 1 ELSE 0 END)) DESC";

        return entityManager.createQuery(query, DefaultExercises.class)
                .setParameter("ids", tagIds).setMaxResults(5)
                .getResultList().stream().map(DefaultExercisesDto::new).collect(Collectors.toList());
    }

}
