package pl.umk.mat.trapp.defaultExercises;

import pl.umk.mat.trapp.sets.Sets;
import pl.umk.mat.trapp.tags.Tag;
import pl.umk.mat.trapp.utils.BaseEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class DefaultExercises extends BaseEntity {
    private String name;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "default_exercises_id")
    private List<Sets> sets = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(
            name = "default_exercise_tags",
            joinColumns = @JoinColumn(name = "default_exercise_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private List<Tag> tags = new ArrayList<>();

    public DefaultExercises() {
        //JPA
    }

    public DefaultExercises(String name, List<Sets> sets, List<Tag> tags) {
        this.name = name;
        this.sets = sets;
        this.tags = tags;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Sets> getSets() {
        return sets;
    }

    public void setSets(List<Sets> sets) {
        this.sets = sets;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }
}
