package pl.umk.mat.trapp.defaultExercises;

import pl.umk.mat.trapp.sets.SetsDto;
import pl.umk.mat.trapp.tags.TagDto;

import java.util.List;
import java.util.stream.Collectors;

public class DefaultExercisesDto {
    private Long id;
    private String name;
    private List<TagDto> tagDto;
    private List<SetsDto> sets;

    public DefaultExercisesDto() {
    }

    public DefaultExercisesDto(Long id, String name, List<TagDto> tagDto, List<SetsDto> sets) {
        this.id = id;
        this.name = name;
        this.tagDto = tagDto;
        this.sets = sets;
    }

    public DefaultExercisesDto(DefaultExercises defaultExercises) {
        this.id = defaultExercises.getId();;
        this.name = defaultExercises.getName();
        this.tagDto = defaultExercises.getTags().stream().map(TagDto::new).collect(Collectors.toList());
        this.sets = defaultExercises.getSets().stream().map(SetsDto::new).collect(Collectors.toList());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TagDto> getTagDto() {
        return tagDto;
    }

    public List<SetsDto> getSets() {
        return sets;
    }

    public void setSets(List<SetsDto> sets) {
        this.sets = sets;
    }
}
