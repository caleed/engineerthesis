package pl.umk.mat.trapp.sizes;

import pl.umk.mat.trapp.utils.BaseRepository;

import java.util.List;

public interface SizesRepository extends BaseRepository<Sizes, Long> {
    List<Sizes> getAllByUserId(Long id);
}
