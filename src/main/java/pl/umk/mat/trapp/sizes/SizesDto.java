package pl.umk.mat.trapp.sizes;

import pl.umk.mat.trapp.enums.BodyParts;
import pl.umk.mat.trapp.user.UserDto;


public class SizesDto {

    private BodyParts bodyParts;
    private double size;
    private UserDto user;
    private double diff;

    public SizesDto(BodyParts bodyParts, Double size, UserDto user, double diff) {
        this.bodyParts = bodyParts;
        this.size = size;
        this.user = user;
        this.diff = diff;
    }

    public SizesDto(Sizes sizes, double diff){
        this.bodyParts = sizes.getBodyParts();
        this.size = sizes.getSize();
        this.user = new UserDto(sizes.getUser());
        this.diff = diff;
    }

    public SizesDto(Sizes sizes){
        this.bodyParts = sizes.getBodyParts();
        this.size = sizes.getSize();
        this.user = new UserDto(sizes.getUser());
        this.diff = 0;
    }

    public BodyParts getBodyParts() {
        return bodyParts;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public UserDto getUser() {
        return user;
    }

    public double getDiff() {
        return diff;
    }
}
