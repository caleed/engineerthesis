package pl.umk.mat.trapp.sizes;

import pl.umk.mat.trapp.enums.BodyParts;
import pl.umk.mat.trapp.user.User;
import pl.umk.mat.trapp.utils.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class Sizes extends BaseEntity {

    private BodyParts bodyParts;

    private double size;

    @ManyToOne
    private User user;

    private double diff;

    public Sizes(BodyParts bodyParts, double size, User user, double diff) {
        this.bodyParts = bodyParts;
        this.size = size;
        this.user = user;
        this.diff = diff;
    }

    public Sizes() {
        this.size = 0;
    }

    public BodyParts getBodyParts() {
        return bodyParts;
    }

    public void setBodyParts(BodyParts bodyParts) {
        this.bodyParts = bodyParts;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public User getUser() {
        return user;
    }

    public void setDiff(double diff) {
        this.diff = diff;
    }

    public double getDiff() {
        return diff;
    }
}
