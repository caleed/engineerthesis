package pl.umk.mat.trapp.sizes;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.umk.mat.trapp.auth.LoggedUserHolder;
import pl.umk.mat.trapp.enums.BodyParts;
import pl.umk.mat.trapp.user.UserDto;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Service
public class SizesService {

    private SizesRepository sizesRepository;
    private final LoggedUserHolder loggedUserHolder;

    @PersistenceContext
    private EntityManager entityManager;

    public SizesService(SizesRepository sizesRepository, LoggedUserHolder loggedUserHolder, EntityManager entityManager) {
        this.sizesRepository = sizesRepository;
        this.loggedUserHolder = loggedUserHolder;
        this.entityManager = entityManager;
    }

    public List<SizesDto> getUserSizes() {
        var loggedUser = loggedUserHolder.getLoggedUser().getUser();
        return sizesRepository.getAllByUserId(loggedUser.getId()).stream().map(SizesDto::new).collect(Collectors.toList());
    }

    @Transactional
    public SizesDto addUserSizes(SizesDto sizesDto) {
        var loggedUser = loggedUserHolder.getLoggedUser().getUser();
        Sizes sizes = new Sizes(
                sizesDto.getBodyParts(),
                sizesDto.getSize(),
                loggedUser,
                0);
        return new SizesDto(sizesRepository.save(sizes));
    }


    public List<SizesDto> getRecentUserSizes(Long start, Long stop) {
        var loggedUser = loggedUserHolder.getLoggedUser().getUser();
        var firstSizes = getFirstSizeBefore( Instant.ofEpochMilli(start));

        var lastSizes = getFirstSizeBefore(Instant.ofEpochMilli(stop));

        return Arrays.stream(BodyParts.values()).map(it -> {
            var firstSize = firstSizes.stream().filter(s -> s.getBodyParts() == it).findAny();
            var lastSize = lastSizes.stream().filter(s -> s.getBodyParts() == it).findAny();

            if (lastSize.isEmpty()) {
                return new SizesDto(it, 0.0, new UserDto(loggedUser), 0);
            }

            if (firstSize.isEmpty()) {
                return new SizesDto( lastSize.get(), 0d);
            }


            return new SizesDto(lastSize.get(), lastSize.get().getSize() - firstSize.get().getSize());

        }).collect(Collectors.toList());
    }

    private List<Sizes> getFirstSizeBefore(Instant date) {
        var loggedUser = loggedUserHolder.getLoggedUser().getUser();
        return entityManager.createQuery(
                " SELECT s FROM Sizes s WHERE (s.bodyParts, s.createdAt) IN (" +
                        "SELECT s2.bodyParts, MAX(s2.createdAt) FROM Sizes s2 WHERE s2.user.id = :userId AND s2.createdAt < :date GROUP BY s2.bodyParts"+
                        " ) ORDER BY s.bodyParts"
                , Sizes.class).setParameter("date", date)
                .setParameter("userId", loggedUser.getId())
                .getResultList();
    }
}