package pl.umk.mat.trapp.sizes;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin()
@RestController
@RequestMapping("/sizes")
public class SizesController {

    private final SizesService sizesService;

    public SizesController(SizesService sizesService) {
        this.sizesService = sizesService;
    }
    
    @GetMapping("/all")
    @ApiOperation(
            value = "Get all sizes from user",
            authorizations = @Authorization(
                    "Authorization"
            )
    )
    public List<SizesDto> getUserSizes() {
        return sizesService.getUserSizes();
    }

    @PostMapping("/add")
    @ApiOperation(
            value = "Get all sizes from user",
            authorizations = @Authorization(
                    "Authorization"
            )
    )
    public SizesDto addUserSize(@RequestBody SizesDto sizesDto) {
        return sizesService.addUserSizes(sizesDto);
    }

    @GetMapping("/allRecent")
    @ApiOperation(
            value = "Get all recent sizes from user",
            authorizations = @Authorization(
                    "Authorization"
            )
    )
    public List<SizesDto> getRecentUserSizes(@RequestParam Long  startDate,@RequestParam Long  stopDate) {
        return sizesService.getRecentUserSizes(startDate, stopDate);
    }



}
