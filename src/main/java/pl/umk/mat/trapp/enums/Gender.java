package pl.umk.mat.trapp.enums;

public enum Gender {
    Male,
    Female,
    Other
}
