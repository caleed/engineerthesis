package pl.umk.mat.trapp.enums;

public enum BodyParts {
    Neck,
    Chest,
    Shoulders,
    Arm,
    Forearm,
    Waist,
    Thigh,
    Calf
}
