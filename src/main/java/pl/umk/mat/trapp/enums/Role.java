package pl.umk.mat.trapp.enums;

public enum Role {
    ADMIN,
    USER,
    TRAINER
}
