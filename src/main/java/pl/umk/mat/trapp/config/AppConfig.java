package pl.umk.mat.trapp.config;

import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    private final String imageDir = "src/main/resources/static/images/";
    private final String imageServerUrl = "/";
    private final String imageRegex = "jpeg|jpg|png|bmp";


    public final static int TOKEN_VALIDITY_IN_DAYS = 7;
    public final static int TOKEN_LENGTH = 10;


    public String getImageDir() {
        return imageDir;
    }

    public String getImageServerUrl() {
        return imageServerUrl;
    }

    public String getImageRegex() {
        return imageRegex;
    }
}
