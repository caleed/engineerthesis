package pl.umk.mat.trapp.auth;

import javax.validation.constraints.NotNull;

public class LoginRequest {
    @NotNull
    private String email;
    @NotNull
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }
}
