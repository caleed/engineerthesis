package pl.umk.mat.trapp.auth;

import net.bytebuddy.utility.RandomString;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.umk.mat.trapp.config.AppConfig;
import pl.umk.mat.trapp.user.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

@Service
public class AuthService {

    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final LoginEntryRepository loginEntryRepository;

    public AuthService(
            AuthenticationManager authenticationManager, UserRepository userRepository,
            PasswordEncoder passwordEncoder, LoginEntryRepository loginEntryRepository
    ) {
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.loginEntryRepository = loginEntryRepository;
    }

    @Transactional
    public AuthResponse login(LoginRequest loginRequest) {
        UserPrincipal p = (UserPrincipal) authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()))
                .getPrincipal();

        var loginEntry = new LoginEntry(
                RandomString.make(AppConfig.TOKEN_LENGTH),
                Instant.now().plus(AppConfig.TOKEN_VALIDITY_IN_DAYS, ChronoUnit.DAYS),
                p.getUser()
        );

        loginEntryRepository.save(loginEntry);

        return createAuthResponse(loginEntry);
    }

    private AuthResponse createAuthResponse(LoginEntry loginEntry) {
        return new AuthResponse(new UserDto(loginEntry.getUser()), loginEntry.getToken());
    }

    @Transactional
    public AuthResponse register(RegisterRequest registerRequest) {
        if (userRepository.existsByEmail(registerRequest.getEmail())) {
            throw new RuntimeException("User already exists");
        }

        var user = new User(
                registerRequest.getFirstName(),
                registerRequest.getLastName(),
                registerRequest.getEmail(),
                passwordEncoder.encode(registerRequest.getPassword()),
                registerRequest.getGender(),
                0
                );

        userRepository.save(user);

        var loginEntry = new LoginEntry(
                RandomString.make(AppConfig.TOKEN_LENGTH),
                Instant.now().plus(AppConfig.TOKEN_VALIDITY_IN_DAYS, ChronoUnit.DAYS),
                user
        );

        loginEntryRepository.save(loginEntry);

        return createAuthResponse(loginEntry);

    }


}
