package pl.umk.mat.trapp.auth;

import pl.umk.mat.trapp.user.UserDto;

public class AuthResponse {
    private UserDto userDto;
    private String token;

    public AuthResponse(UserDto userDto, String token) {
        this.userDto = userDto;
        this.token = token;
    }

    public UserDto getUserDto() {
        return userDto;
    }

    public String getToken() {
        return token;
    }
}
