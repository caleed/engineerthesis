package pl.umk.mat.trapp.auth;

import pl.umk.mat.trapp.enums.Gender;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class RegisterRequest {
    @NotBlank
    @Email
    private String email;

    private String firstName;

    private String lastName;

    private Gender gender;

    @NotBlank
    @Size(min = 8, max = 50)
    private String password;

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Gender getGender() {
        return gender;
    }

}
