package pl.umk.mat.trapp.auth;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import pl.umk.mat.trapp.config.AppConfig;
import pl.umk.mat.trapp.exceptions.AuthException;
import pl.umk.mat.trapp.user.LoginEntry;
import pl.umk.mat.trapp.user.LoginEntryRepository;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

@Component
public class AuthFilter extends OncePerRequestFilter {

    private final UserDetailsServiceImp userDetailsService;
    private final LoginEntryRepository loginEntryRepository;

    public AuthFilter(UserDetailsServiceImp userDetailsService, LoginEntryRepository loginEntryRepository) {
        this.userDetailsService = userDetailsService;
        this.loginEntryRepository = loginEntryRepository;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String token = getTokenFromRequest(request);
        if (token != null) {
            var loginEntry = loginEntryRepository.findByToken(token)
                    .orElseThrow(() -> new AuthException("Token not found"));
            if (loginEntry.isExpired()) {
                throw new AuthException("Token expired");
            }
            try {
                UserDetails userDetails = userDetailsService.loadUserByUsername(loginEntry.getUser().getEmail());
                extendTokenValidity(loginEntry);
                SecurityContextHolder.getContext().setAuthentication(new TokenBasedAuthentication(userDetails, token));

            } catch (UsernameNotFoundException e) {
                throw new AuthException(e.getMessage());
            }
        }
        filterChain.doFilter(request, response);
    }

    private void extendTokenValidity(LoginEntry loginEntry) {
        loginEntry.setExpireAt(Instant.now().plus(AppConfig.TOKEN_VALIDITY_IN_DAYS, ChronoUnit.DAYS));
    }

    private String getTokenFromRequest(HttpServletRequest request) {
        return request.getHeader("Authorization");
    }

}
