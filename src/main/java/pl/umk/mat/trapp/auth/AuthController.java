package pl.umk.mat.trapp.auth;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
public class AuthController {

    private final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }
    @CrossOrigin()
    @PostMapping("/register")
    public AuthResponse register(@RequestBody @Valid RegisterRequest  registerRequest){
        return authService.register(registerRequest);
    }

    @CrossOrigin()
    @PostMapping("/login")
    public AuthResponse login(@RequestBody @Valid LoginRequest  loginRequest){
        return authService.login(loginRequest);
    }


}
